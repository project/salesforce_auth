<?php

/**
 * @file
 * This module give attempt to login in drupal with Salesforce REST API.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_init().
 */
function salesforce_auth_init() {

}

/**
 * {@inheritdoc}
 *
 * Generating Login with Salesforce button and use into user_login_form_alter.
 */
function generate_login_link() {
  $config = \Drupal::config('salesforce_auth.settings');

  $sf_consumer_key = $config->get('salesforce_consumer_key');
  $sf_callback_uri = $config->get('salesforce_callback_uri');
  $sf_login_uri = $config->get('salesforce_login_uri');

  if (!empty($sf_login_uri['salesforce_login_uri']) ||
    !empty($sf_callback_uri['salesforce_callback_uri'])) {
    $login_uri = $sf_login_uri['salesforce_login_uri'] . "/services/oauth2/authorize?response_type=code&prompt=login&client_id="
    . $sf_consumer_key['salesforce_consumer_key'] . "&redirect_uri=" . urlencode($sf_callback_uri['salesforce_callback_uri']);

    return "<a href='$login_uri' class='btn btn-primary'> Log in with Salesforce </a>";
  }
}

/**
 * Implements hook_form_alter().
 */
function salesforce_auth_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  switch ($form_id) {
    case 'user_login_form':
      // Add in some CSS.
      $form['#attached']['library'][] = 'salesforce_auth/salesforce_auth_css';

      $form['name']['#attributes']['placeholder'] = t('Username/Email');
      $form['password']['#attributes']['placeholder'] = t('Password');

      $form['login_with_salesforce'] = [
        '#prefix' => '<div>' . generate_login_link(),
        '#suffix' => '</div>',
        '#weight' => -1000,
      ];
      break;
  }
}

/**
 * Implements hook_user_delete().
 */
function salesforce_auth_user_delete($account) {
  db_delete('fs_authmap')->condition('uid', $account->id())->execute();
  db_delete('fs_salesforce_auth')->condition('uid', $account->id())->execute();
}

/**
 * Implements hook_user_logout().
 */
function salesforce_auth_user_logout($account) {
  $ssoToken = $_SESSION['salesforce_sso'];
  $sf_login_uri = variable_get('sf_login_url');
  if (isset($sf_login_uri) && !empty($sf_login_uri)) {
    requestLogout($ssoToken);
    session_destroy();
  }
  drupal_goto();
  drupal_set_message(t('Thanks for visiting, come again!'));
}
