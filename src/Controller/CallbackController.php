<?php

namespace Drupal\salesforce_auth\Controller;

use Drupal\user\Entity\User;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\MetadataBubblingUrlGenerator;
use Drupal\salesforce_auth\Rest\RestClientInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CallbackController.
 *
 * Provides route responses for the Example module.
 */
class CallbackController extends ControllerBase {

  /**
   * The GuzzleHttp\Client service.
   *
   * @var client
   */
  protected $client;

  /**
   * The http client service.
   *
   * @var httpClient
   */
  protected $httpClient;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connections.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The user service for private tempstore.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $privateTempstore;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    RestClientInterface $rest,
    Client $http_client,
    MetadataBubblingUrlGenerator $url_generator,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    User $private_tempstore,
    LanguageManagerInterface $language_manager,
    MessengerInterface $messenger = NULL,
    Request $request
  ) {
    $this->client = $rest;
    $this->httpClient = $http_client;
    $this->urlGenerator = $url_generator;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->privateTempstore = $private_tempstore;
    $this->languageManager = $language_manager;
    $this->messenger = $messenger;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('salesforce_auth.client'),
      $container->get('http_client'),
      $container->get('url_generator'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('user.private_tempstore'),
      $container->get('language_manager'),
      $container->get('messenger', ContainerInterface::NULL_ON_INVALID_REFERENCE),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function successMessage() {
    $this->messenger->addMessage(new TranslatableMarkup('Successfully connected to %endpoint', ['%endpoint' => $this->client->getInstanceUrl()]));
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function callback() {
    global $user;

    $language = $this->languageManager->getCurrentLanguage()->getId();
    $user = User::create();

    // If no code is provided, return access denied.
    if (empty($this->request->query->get('code'))) {
      throw new AccessDeniedHttpException();
    }

    $pass = '';
    $code = $this->request->query->get('code');
    $response = $this->requestToken($code);
    $validateClient = $this->client->handleAuthResponse($response);
    $token = $validateClient['access_token'];

    if ($token) {
      $custInfo = $this->client->initializeIdentity($validateClient['id']);
      $active = $custInfo['active'];

      // If ($verified == 1 && $active == 1)
      if ($active == 1) {
        $user_id = $custInfo['user_id'];
        $uname = $custInfo['username'];
        $email = $custInfo['email'];
        $user_type = $custInfo['user_type'];
        $nicknm = $custInfo['nick_name'];
        $displaynm = $custInfo['display_name'];

        $uid = $this->getUidFromUsername($uname);
        $externalid = $this->getExternalUidFromUsername($uname);
        $user_by_email = user_load_by_mail($uname);

        if (($externalid || !$uid) && ((!$uid && !$user_by_email) || (isset($user_by_email->get('uid')->value) && $uid == $user_by_email->get('uid')->value && $externalid))) {
          $existing_uid = $this->getUidFromSfuserid($user_id);
          $existing_user = NULL;

          if ($existing_uid) {
            // Load the existing user.
            $existing_user = $this->entityTypeManager->getStorage('user')->load($existing_uid);
            $existing_user->get('name')->value = $uname;
            $existing_user->get('mail')->value = $email;

            // update/save the existing user.
            $existing_user->setUsername($uname);
            $existing_user->setPassword($pass);
            $existing_user->setEmail($email);
            $existing_user->addRole('rid');
            $existing_user->save();

            // Update the fs_authmap fields.
            $this->updateAalesforceAuthmap($uname, $existing_uid);

            // Update the fs_salesforce_auth fields.
            $this->updateSalesforceAuth($user_id, $nicknm, $displaynm, $existing_uid);

            // Assign user_id to the varible.
            $uid = $existing_user->get('uid')->value;
          }
          else {
            // Register the user.
            $user->setUsername($uname);
            $user->setPassword($pass);
            $user->setEmail($email);
            $user->enforceIsNew();
            $user->set("init", $uname);
            $user->set("langcode", $language);
            $user->set("preferred_langcode", $language);
            $user->set("preferred_admin_langcode", $language);
            $user->addRole('rid');
            $user->activate();
            $user->save();

            // Insert into the fs_authmap table.
            $this->setSalesforceAuthmap($user->id(), $uname, 'salesforce_auth');

            // Insert into the salesforce_auth table.
            $this->setSalesforceAuth($user->id(), $user_id, $user_type, $nicknm, $displaynm);

            // Save user email and load user object.
            $newUser = $this->entityTypeManager->getStorage('user')->load($user->id());
            $newUser->get('name')->value = $uname;
            $newUser->get('pass')->value = $pass;

            // Assign user_id to the varible.
            $uid = $newUser->get('uid')->value;
          }
        }
        // Load the user and let login user.
        $user_load = $this->entityTypeManager->getStorage('user')->load($uid);
        user_login_finalize($user_load);

        // Set sso token in session.
        $tempstore = $this->privateTempstore->get('salesforce_auth');
        $tempstore->set('sf_token', $token);
        $tempstore->set('sf_userid', $user_id);
      }
    }
    return Url::fromRoute('<front>');
  }

  /**
   * Get consumer key for salesforce API.
   */
  public function getConsumerKey() {
    $config = $this->config('salesforce_auth.settings');
    $custInfo = $config->get('salesforce_consumer_key');
    return $custInfo['salesforce_consumer_key'];
  }

  /**
   * Get consumer secret key for salesforce API.
   */
  public function getConsumerSecret() {
    $config = $this->config('salesforce_auth.settings');
    $custInfo = $config->get('salesforce_consumer_secret');
    return $custInfo['salesforce_consumer_secret'];
  }

  /**
   * Get callback uri for salesforce API.
   */
  public function getCallbackUrl() {
    $config = $this->config('salesforce_auth.settings');
    $custInfo = $config->get('salesforce_callback_uri');
    return $custInfo['salesforce_callback_uri'];
  }

  /**
   * Get login uri for salesforce API.
   */
  public function getLoginUrl() {
    $config = $this->config('salesforce_auth.settings');
    $custInfo = $config->get('salesforce_login_uri');
    return $custInfo['salesforce_login_uri'];
  }

  /**
   * Generate auth token uri from login uri for salesforce API.
   */
  public function getAuthTokenUrl() {
    return $this->getLoginUrl() . '/services/oauth2/token';
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function requestToken($code) {
    $url = $this->getAuthTokenUrl();
    $headers = [
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];

    $data = urldecode(UrlHelper::buildQuery([
      'code' => $code,
      'grant_type' => 'authorization_code',
      'client_id' => $this->getConsumerKey(),
      'client_secret' => $this->getConsumerSecret(),
      'redirect_uri' => $this->getCallbackUrl(),
    ]));
    $response = $this->httpClient->post($url, ['headers' => $headers, 'body' => $data]);
    return $response;
  }

  /**
   * Fetching field values from different tables with id/name/mail/authname.
   *
   * @param string $uname
   *   The user name.
   */
  public function getUidFromUsername($uname) {
    $query = $this->connection->select('users_field_data', 'ufd');
    $query->addField('ufd', 'uid');
    $query->condition('ufd.name', $uname);
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   *
   * @param string $uname
   *   The user name.
   */
  public function getExternalUidFromUsername($uname) {
    $query = $this->connection->select('fs_authmap', 'fsam');
    $query->addField('fsam', 'uid');
    $query->condition('fsam.authname', $uname);
    $query->condition('fsam.module', 'salesforce_auth');
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   *
   * @param int $userid
   *   The user id.
   */
  public function getUidFromSfuserid($userid) {
    $query = $this->connection->select('fs_salesforce_auth', 'fssa');
    $query->addField('fssa', 'uid');
    $query->condition('fssa.user_id', $userid);
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   *
   * @param int $uid
   *   The user id.
   * @param string $name
   *   The user name.
   * @param string $module
   *   The module which is using.
   */
  public function setSalesforceAuthmap($uid, $name, $module) {
    $query = $this->connection->insert('fs_authmap');
    $query->fields(['uid', 'authname', 'module']);
    $query->values([$uid, $name, $module]);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @param int $uid
   *   The uid.
   * @param int $userid
   *   The user id.
   * @param string $utype
   *   The user type.
   * @param string $nicknm
   *   The nick name.
   * @param string $displaynm
   *   The display name.
   */
  public function setSalesforceAuth($uid, $userid, $utype, $nicknm, $displaynm) {
    $query = $this->connection->insert('fs_salesforce_auth');
    $query->fields([
      'uid',
      'user_id',
      'user_type',
      'user_nickname',
      'user_displayname',
    ]);
    $query->values([$uid, $userid, $utype, $nicknm, $displaynm]);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @param string $name
   *   The user name.
   * @param int $uid
   *   The user id.
   */
  public function updateSalesforceAuthmap($name, $uid) {
    $query = $this->connection->update('fs_authmap');
    $query->fields(['authname' => $name]);
    $query->condition('uid', $uid);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @param int $userid
   *   The user id.
   * @param string $nicknm
   *   The nick name.
   * @param string $displaynm
   *   The display name.
   * @param int $uid
   *   The uid.
   */
  public function updateSalesforceAuth($userid, $nicknm, $displaynm, $uid) {
    $query = $this->connection->update('fs_salesforce_auth');
    $query->fields([
      'user_id' => $userid,
      'user_nickname' => $nicknm,
      'user_displayname' => $displaynm,
    ]);
    $query->condition('uid', $uid);
    $query->execute();
  }

}
