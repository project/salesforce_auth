<?php

namespace Drupal\salesforce;

/**
 * Class Select Query Result.
 */
class SelectQueryResult {

  /**
   * Get totalSize of the query.
   *
   * @var totalSize
   */
  protected $totalSize;

  /**
   * Set done.
   *
   * @var done
   */
  protected $done;

  /**
   * Getting records.
   *
   * @var records
   */
  protected $records;

  /**
   * The next record url.
   *
   * @var nextRecordsUrl
   */
  protected $nextRecordsUrl;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $results) {
    $this->totalSize = $results['totalSize'];
    $this->done = $results['done'];
    if (!$this->done) {
      $this->nextRecordsUrl = $results['nextRecordsUrl'];
    }
    $this->records = [];
    foreach ($results['records'] as $record) {
      $this->records[$record['Id']] = new SObject($record);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function nextRecordsUrl() {
    return $this->nextRecordsUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function size() {
    return $this->totalSize;
  }

  /**
   * {@inheritdoc}
   */
  public function done() {
    return $this->done;
  }

  /**
   * {@inheritdoc}
   */
  public function records() {
    return $this->records;
  }

  /**
   * {@inheritdoc}
   */
  public function record(SFID $id) {
    if (!isset($this->records[(string) $id])) {
      throw new \Exception('No record found');
    }
    return $this->records[(string) $id];
  }

}
