<?php

namespace Drupal\salesforce_auth\Rest;

use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class RestException.
 */
class RestException extends \RuntimeException implements ExceptionInterface {

  /**
   * The rest response.
   *
   * @var response
   */
  protected $response;

  /**
   * {@inheritdoc}
   */
  public function __construct(ResponseInterface $response = NULL, $message = "", $code = 0, \Exception $previous = NULL) {
    $this->response = $response;
    $message .= $this->getResponseBody();
    parent::__construct($message, $code, $previous);
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseBody() {
    if (!$this->response) {
      return;
    }
    $body = $this->response->getBody();
    if ($body) {
      return $body->getContents();
    }
    return '';
  }

}
